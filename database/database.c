#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 12345
#define MAX_BUFFER_SIZE 1024

typedef struct {
    char username[50];
    char password[50];
} User;

User users[100]; 
int user_count = 0; 

void process_request(int client_socket, char *request) {
    char response[MAX_BUFFER_SIZE];
    
    if (strncmp(request, "CREATE USER", 11) == 0) {
        char username[50], password[50];
        if (sscanf(request, "CREATE USER %s IDENTIFIED BY %s", username, password) == 2) {
            if (geteuid() == 0) {
                if (user_count < sizeof(users) / sizeof(User)) {
                    strcpy(users[user_count].username, username);
                    strcpy(users[user_count].password, password);
                    user_count++;
                    snprintf(response, sizeof(response), "User '%s' created.\n", username);
                    printf("User '%s' created.\n", username);
                    
                    if (seteuid(user_count) != 0) {
                        perror("Error setting effective user ID");
                        exit(EXIT_FAILURE);
                    }
                    snprintf(response, sizeof(response), "User switched to '%s'.\n", username);
                    printf("User switched to '%s'.\n", username);
                } else {
                    snprintf(response, sizeof(response), "Maximum number of users reached.\n");
                    printf("Maximum number of users reached.\n");
                }
            } else {
                snprintf(response, sizeof(response), "Access denied. Only root can create new users.\n");
                printf("Access denied. Only root can create new users.\n");
            }

        } else {
            snprintf(response, sizeof(response), "Invalid CREATE USER command.\n");
            printf("Invalid CREATE USER command received.\n");
        }

    } else {
        int is_root = (geteuid() == 0);
        char username[50], password[50];
        sscanf(request, "-u %s -p %s", username, password);

        if (is_root || (strcmp(username, "grk") == 0 && geteuid() == 0)) {
        
            snprintf(response, sizeof(response), "Access granted. All database access.\n");
            printf("User %s has access to all databases.\n", username);
            int user_number;

            if (sscanf(username, "%d", &user_number) == 1 && user_number > 1 && user_number <= user_count) {

                if (seteuid(user_number) != 0) {
                    perror("Error setting effective user ID");
                    exit(EXIT_FAILURE);
                }

                snprintf(response, sizeof(response), "User switched to '%s'.\n", username);
                printf("User switched to '%s'.\n", username);
            }

        } else {
            snprintf(response, sizeof(response), "Access granted. User-specific database access.\n");
            printf("User %s has access to their specific database.\n", username);
        }
    }
    send(client_socket, response, sizeof(response), 0);
}

int main() {
    if (geteuid() != 0) {
        fprintf(stderr, "Error: Server must be run as root (sudo).\n");
        exit(EXIT_FAILURE);
    }

    int server_socket, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);
    server_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (server_socket == -1) {
        perror("Error creating socket");
        exit(EXIT_FAILURE);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error binding socket");
        exit(EXIT_FAILURE);
    }

    listen(server_socket, 5);
    printf("Server listening on port %d...\n", PORT);

    while (1) {
        client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
        if (client_socket == -1) {
            perror("Error accepting connection");
            exit(EXIT_FAILURE);
        }

        char buffer[MAX_BUFFER_SIZE];
        recv(client_socket, buffer, sizeof(buffer), 0);
        printf("Received data from client: %s\n", buffer);
        process_request(client_socket, buffer);
        close(client_socket);

    }

    close(server_socket);
    return 0;
}
