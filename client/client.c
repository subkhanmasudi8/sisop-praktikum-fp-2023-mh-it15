#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 12345
#define MAX_BUFFER_SIZE 1024

void send_request(int client_socket, const char *request) {
    char buffer[MAX_BUFFER_SIZE];
    snprintf(buffer, sizeof(buffer), "%s", request);
    send(client_socket, buffer, strlen(buffer), 0);
    printf("Sent data to server: %s\n", buffer);

}

int main(int argc, char *argv[]) {
    int is_root = (geteuid() == 0);
    char server_program[50];

    if (is_root) {
        snprintf(server_program, sizeof(server_program), "./database");
    } else {
        snprintf(server_program, sizeof(server_program), "sudo ./database");
    }

    int client_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (client_socket == -1) {
        perror("Error creating socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    server_addr.sin_port = htons(PORT);

    if (connect(client_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error connecting to server");
        exit(EXIT_FAILURE);
    }

    while (1) {
        if (is_root) {
            printf("Enter command: ");
            char user_input[MAX_BUFFER_SIZE];
            fgets(user_input, sizeof(user_input), stdin);
            send_request(client_socket, user_input);
            char buffer[MAX_BUFFER_SIZE];
            recv(client_socket, buffer, sizeof(buffer), 0);
            printf("Received response from server: %s\n", buffer);
            char *newline = strchr(user_input, '\n');

            if (newline != NULL) {

                *newline = '\0';
            }

            if (strcmp(user_input, "exit") == 0) {
                break;
            }
        } else {
            fprintf(stderr, "Error: Client must be run as root (sudo).\n");
            break;
        }
    }
    close(client_socket);
    return 0;

}
