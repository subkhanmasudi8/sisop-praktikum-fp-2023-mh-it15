# sisop-praktikum-modul-fp-2023-MH-IT15
Laporan Resmi pengerjaan soal Final Praktikum Sistem Operasi 2023 Kelompok IT15

## Anggota Kelompok
1. Ilhan Ahmad Syafa (5027221040)
2. Subkhan Masudi (5027221044)
3. Gilang Raya Kurniawan (5027221045)

# SOAL 
**SISTEM DATABASE SEDERHANA**

**Bagaimana Program Diakses**
- Program server berjalan sebagai daemon.
- Untuk bisa akses console database, perlu buka program client (kalau di Linux seperti command mysql di bash).
- Program client dan utama berinteraksi lewat socket.
- Program client bisa mengakses server dari mana saja.

**Struktur Direktori**
```
# Struktur repository (di repository Gitlab)
-database/
--[program_database].c
--[settingan_cron_backup]
-client/
--[program_client].c
-dump/
--[program_dump_client].c
```

**Bagaimana Database Digunakan**

**A. Autentikasi**

- Terdapat user dengan **username** dan **password**  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan **root (sudo)**, maka bisa mengakses semua database yang ada.\
**Catatan:** Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.
```
# Format

./[program_client_database] -u [username] -p [password]

# Contoh

./client_databaseku -u john -p john123
```
- **Username**, **password**, dan **hak akses database** disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan **root**.
- User **root** sudah ada dari awal.
```
# Contoh cara user root mengakses program client

sudo ./client_database
```

- Menambahkan user (Hanya bisa dilakukan user **root**)
```
# Format

CREATE USER [nama_user] IDENTIFIED BY [password_user];

# Contoh

CREATE USER khonsu IDENTIFIED BY khonsu123;

```

**B. Autorisasi**

- Untuk dapat mengakses _database_ yang dia punya, _permission_ dilakukan dengan _command_. Pembuatan tabel dan semua DML butuh untuk mengakses _database_ terlebih dahulu.
```
# Format

USE [nama_database];

# Contoh

USE database1;

```

- Yang bisa memberikan _permission_ atas _database_ untuk suatu user hanya **root**.
```
# Format

GRANT PERMISSION [nama_database] INTO [nama_user];

# Contoh

GRANT PERMISSION database1 INTO user1;

```
- _User_ hanya bisa mengakses _database_ di mana dia diberi _permission_ untuk _database_ tersebut.

**C. Data Definition Language**

- Input penamaan _database_, tabel, dan kolom hanya angka dan huruf.
- Semua _user_ bisa membuat _database_, otomatis _user_ tersebut memiliki permission untuk database tersebut.


```

# Format


CREATE DATABASE [nama_database];

# Contoh

CREATE DATABASE database1;

```
- **Root** dan _user_ yang memiliki _permission_ untuk suatu _database_ untuk bisa membuat tabel untuk _database_ tersebut, tentunya setelah mengakses _database_ tersebut. Tipe data dari semua kolom adalah **string** atau **integer**. Jumlah kolom bebas.
```

# Format

CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);


# Contoh

CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

```
- Bisa melakukan **DROP _database_**, _table_ (setelah mengakses _databas_e), dan kolom. Jika sasaran _drop_ ada maka _di-drop_, jika tidak ada maka biarkan.
```

# Format

DROP [DATABASE | TABLE | COLUMN] [nama_database | nama_tabel | [nama_kolom] FROM [nama_tabel]];


# Contoh

## Drop database

DROP DATABASE database1;

## Drop table

DROP TABLE table1;

## Drop Column

DROP COLUMN kolom1 FROM table1;
```
**D. Data Manipulation Language**
-  **INSERT**\
Hanya bisa _insert_ satu _row_ per satu _command_. _Insert_ sesuai dengan jumlah dan urutan kolom.

```
# Format

INSERT INTO [nama_tabel] ([value], ...);


# Contoh

INSERT INTO table1 (‘value1’, 2, ‘value3’, 4);

```
- **UPDATE**\
Hanya bisa **update** satu kolom per satu _command_.
```
# Format

UPDATE [nama_tabel] SET [nama_kolom]=[value];


# Contoh

UPDATE table1 SET kolom1=’new_value1’;


```

- **DELETE**
_Delete_ data yang ada di tabel.
```
# Format

DELETE FROM [nama_tabel];


# Contoh

DELETE FROM table1;
```


- **SELECT**
```
# Format

SELECT [nama_kolom, … | *] FROM [nama_tabel];


# Contoh 1

SELECT kolom1, kolom2 FROM table1;

# Contoh 2

SELECT * FROM table1;
```


- **WHERE**
_Command_ **UPDATE**, **SELECT**, dan **DELETE** bisa dikombinasikan dengan **WHERE**. **WHERE** hanya untuk satu kondisi. Dan hanya ‘=’.
```
# Format

[Command UPDATE, SELECT, DELETE] WHERE [nama_kolom]=[value];


# Contoh

DELETE FROM table1 WHERE kolom1=’value1’;
```

**E. Logging**

- Setiap _command_ yang dipakai harus dilakukan _logging_ ke suatu _file_ dengan format. Jika yang eksekusi **root**, maka _username_ **root**.
```
# Format di dalam log

timestamp(yyyy-mm-dd hh:mm:ss):username:command

	
# Contoh

2021-05-19 02:05:15:khonsu:SELECT FROM table1
```

**F. Reliability**

- Harus membuat suatu program terpisah untuk _dump database_ ke _command-command_ yang akan di **print ke layar**. Untuk memasukkan ke _file_, gunakan **_redirection_**. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai _database_ level saja, tidak perlu sampai tabel. 
```
# Format

./[program_dump_database] -u [username] -p [password] [nama_database]


# Contoh

./databasedump -u khonsu -p khonsu123 database1 > database1.backup
```

Contoh hasil isi _file_ **database1.backup** adalah sebagai berikut.
```
DROP TABLE table1;
CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);

DROP TABLE table2;
CREATE TABLE table2 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
```

- Program **dump** database **dijalankan tiap jam** untuk semua _database_ dan _log_, lalu di **zip** sesuai _timestamp_, lalu _log_ dikosongkan kembali.

**G. Tambahan**

- Kita bisa memasukkan _command_ lewat _file_ dengan _redirection_ di program _client_. 
```
# Format

./[program_client_database] -u [username] -p [password] -d [database] < [file_command]


# Contoh

./client_databaseku -u john -p john123 -d database1 < database.backup
```

**H. Error Handling**
- Jika ada _command_ yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan _error_ **tanpa keluar dari program client**.

## Solution Soal
[Source Code](https://gitlab.com/subkhanmasudi8/sisop-praktikum-fp-2023-mh-it15)

## Penjelasan Soal

- Pada program **client**, hanya berfungsi untuk melakukan koneksi dengan **server** atau **database** dan juga melakukan pengecekan terkait siapa yang mengakses **database** (_user _root__ atau _user biasa_)
- Sedangkan pada program **database**, ini yang merupakan inti dari sebuah sistemnya. Program ini menampung database, nama user dan password yang digunakan dalam sistem ini dengan struktur yang telah diatur sedemikian rupa. Selain itu, program ini juga terhubung dengan program **client** dan menangani request yang dilakukan oleh _user root_ dan _user biasa_ seperti **CREATE USER** dan **GRANT PERMISSION** oleh _user root_ serta _command-command_ terkait DDL dan DML lain yang telah tertera pada soal di atas.

## Pengerjaan Soal
**A. Autentikasi**
1. Nyalakan _server_ atau _database_ dan cobalah membuat _user baru_ dengan menggunakan _user root_ yang sudah didefine _username_ dan _passwordnya_ pada program **database**

<a href="https://ibb.co/yXPBDPQ"><img src="https://i.ibb.co/6mwJ2wW/nyalakan.png" alt="nyalakan" border="0"></a>

<a href="https://ibb.co/RQcncbp"><img src="https://i.ibb.co/frHRHFY/CREATE-USER.png" alt="CREATE-USER" border="0"></a>

**B. Autorisasi**

2. Berikan akses _database_ kepada _user_ yang dikehendaki (yang sudah dibuat sebelumnya)

<a href="https://ibb.co/2ZDS1Zk"><img src="https://i.ibb.co/Sfhc4f3/GRANT.png" alt="GRANT" border="0"></a>

<a href="https://ibb.co/dpVtn1z"><img src="https://i.ibb.co/my20Q1M/USE.png" alt="USE" border="0"></a>

## Kendala
- Belum mengimplementasikan algoritma skenario DDL dan DML.
- Terminal client pada beberapa kali _command_ akan mati. Namun, sudah mengimplementasikan _error handling_ (**poin H**). Ketika baru pertama kali dinyalakan meskipun ada kesalahan dalam menginput _command_, _nama user_ maupun _database_, terminal akan masih tetap menyala.
- Belum mengimplementasikan poin **E**, **F** dan **G**.
